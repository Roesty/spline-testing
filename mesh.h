#ifndef MESH_CLASS_H
#define MESH_CLASS_H

#include<string>

#include "vao.h"
#include "ebo.h"
#include "camera.h"
#include "texture.h"

class Mesh
{
public:
    glm::vec4 position;

    std::vector <Vertex> vertices;
    std::vector <GLuint> indices;
    std::vector <Texture> textures;
    // Store VAO in public so it can be used in the Draw function
    VAO m_vao;

    // Initializes the mesh
    Mesh(std::vector <Vertex>& vertices, std::vector <GLuint>& indices, std::vector <Texture>& textures);
    Mesh();

    // Draws the mesh
    void Draw(Shader& shader, Camera& camera);
};
#endif
