################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../camera.cpp \
../circle.cpp \
../ebo.cpp \
../main.cpp \
../mesh.cpp \
../meshfactory.cpp \
../shader.cpp \
../stb.cpp \
../testclass.cpp \
../texture.cpp \
../vao.cpp \
../vbo.cpp 

CPP_DEPS += \
./camera.d \
./circle.d \
./ebo.d \
./main.d \
./mesh.d \
./meshfactory.d \
./shader.d \
./stb.d \
./testclass.d \
./texture.d \
./vao.d \
./vbo.d 

OBJS += \
./camera.o \
./circle.o \
./ebo.o \
./main.o \
./mesh.o \
./meshfactory.o \
./shader.o \
./stb.o \
./testclass.o \
./texture.o \
./vao.o \
./vbo.o 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean--2e-

clean--2e-:
	-$(RM) ./camera.d ./camera.o ./circle.d ./circle.o ./ebo.d ./ebo.o ./main.d ./main.o ./mesh.d ./mesh.o ./meshfactory.d ./meshfactory.o ./shader.d ./shader.o ./stb.d ./stb.o ./testclass.d ./testclass.o ./texture.d ./texture.o ./vao.d ./vao.o ./vbo.d ./vbo.o

.PHONY: clean--2e-

