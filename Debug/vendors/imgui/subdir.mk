################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../vendors/imgui/imgui.cpp \
../vendors/imgui/imgui_demo.cpp \
../vendors/imgui/imgui_draw.cpp \
../vendors/imgui/imgui_impl_glfw_gl3.cpp 

CPP_DEPS += \
./vendors/imgui/imgui.d \
./vendors/imgui/imgui_demo.d \
./vendors/imgui/imgui_draw.d \
./vendors/imgui/imgui_impl_glfw_gl3.d 

OBJS += \
./vendors/imgui/imgui.o \
./vendors/imgui/imgui_demo.o \
./vendors/imgui/imgui_draw.o \
./vendors/imgui/imgui_impl_glfw_gl3.o 


# Each subdirectory must supply rules for building sources it contributes
vendors/imgui/%.o: ../vendors/imgui/%.cpp vendors/imgui/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-vendors-2f-imgui

clean-vendors-2f-imgui:
	-$(RM) ./vendors/imgui/imgui.d ./vendors/imgui/imgui.o ./vendors/imgui/imgui_demo.d ./vendors/imgui/imgui_demo.o ./vendors/imgui/imgui_draw.d ./vendors/imgui/imgui_draw.o ./vendors/imgui/imgui_impl_glfw_gl3.d ./vendors/imgui/imgui_impl_glfw_gl3.o

.PHONY: clean-vendors-2f-imgui

