#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>

#include <chrono>
#include <ctime>
#include <random>

#include "vendors/imgui/imgui.h"
#include "vendors/imgui/imgui_internal.h"
#include "vendors/imgui/imgui_impl_glfw_gl3.h"

#include "circle.h"
#include "meshfactory.h"
#include "texture.h"

#define GLM_ENABLE_EXPERIMENTAL

const unsigned int width = 2400;
const unsigned int height = 1300;

enum eColors {
    WHITE=0,
    RED,
    GREEN,
    BLUE,
    BLACK,

    MAX_ECOLORS
};

// Vertices coordinates
Vertex vertices[] =
{
    //               COORDINATES                    NORMALS                     COLOR                         TEX COORDS
    Vertex{glm::vec3(-1.0f, 0.0f,  1.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(0.0f, 0.0f)},
    Vertex{glm::vec3(-1.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(0.0f, 1.0f)},
    Vertex{glm::vec3( 1.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(1.0f, 1.0f)},
    Vertex{glm::vec3( 1.0f, 0.0f,  1.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(1.0f, 0.0f)}
};

// Indices for vertices order
GLuint indices[] =
{
    0, 1, 2,
    0, 2, 3
};

Vertex lightVertices[] =
{ //     COORDINATES     //
    Vertex{glm::vec3(-0.1f, -0.1f,  0.1f)},
    Vertex{glm::vec3(-0.1f, -0.1f, -0.1f)},
    Vertex{glm::vec3(0.1f, -0.1f, -0.1f)},
    Vertex{glm::vec3(0.1f, -0.1f,  0.1f)},
    Vertex{glm::vec3(-0.1f,  0.1f,  0.1f)},
    Vertex{glm::vec3(-0.1f,  0.1f, -0.1f)},
    Vertex{glm::vec3(0.1f,  0.1f, -0.1f)},
    Vertex{glm::vec3(0.1f,  0.1f,  0.1f)}
};

GLuint lightIndices[] =
{
    0, 1, 2,
    0, 2, 3,
    0, 4, 7,
    0, 7, 3,
    3, 7, 6,
    3, 6, 2,
    2, 6, 5,
    2, 5, 1,
    1, 5, 4,
    1, 4, 0,
    4, 5, 6,
    4, 6, 7
};

std::string projectName = "GeometryMorphing";

std::string getModelPath(std::string modelName)
{
    return "../"+projectName+"/resources/models/"+modelName+"/scene.gltf";
}

std::string getShaderPath(std::string shaderName)
{
	return "../resources/shaders/"+shaderName;
    return "../"+projectName+"/resources/shaders/"+shaderName;
}

std::string getTexturePath(std::string textureName)
{
	return "../resources/textures/"+textureName+".png";
    return "../"+projectName+"/resources/textures/"+textureName+".png";
}

int main()
{
    srand(std::chrono::system_clock::now().time_since_epoch().count());

    std::random_device rd;     // only used once to initialise (seed) engine
    std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)

    std::vector<glm::vec3> COLORS;
    COLORS.push_back({1, 1, 1});
    COLORS.push_back({1, 0, 0});
    COLORS.push_back({0, 1, 0});
    COLORS.push_back({0, 0, 1});
    COLORS.push_back({0, 0, 0});

    // Initialize GLFW
    glfwInit();

    // Tell GLFW what version of OpenGL we are using
    // In this case we are using OpenGL 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    // Tell GLFW we are using the CORE profile
    // So that means we only have the modern functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create a GLFWwindow object of 800 by 800 pixels, naming it "YoutubeOpenGL"
    GLFWwindow* window = glfwCreateWindow(width, height, "YoutubeOpenGL", NULL, NULL);
    // Error check if the window fails to create
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    // Introduce the window into the current context
    glfwMakeContextCurrent(window);

    ///Imgui setup
    //Setup ImGui binding
    ImGui::CreateContext();
    ImGui_ImplGlfwGL3_Init(window, true);

    //Setup style
    ImGui::StyleColorsDark();

    //Load GLAD so it configures OpenGL
    glewInit();
    // Specify the viewport of OpenGL in the Window
    // In this case the viewport goes from x = 0, y = 0, to x = 800, y = 800
    glViewport(0, 0, width, height);

    std::string path1 = getTexturePath("planks");
    std::string path2 = getTexturePath("planksSpec");

    std::cout<<path1<<std::endl;
    std::cout<<path2<<std::endl;

    // Texture data
    Texture textures[]
    {
        Texture(path1.c_str(), "diffuse", 0, GL_RGBA, GL_UNSIGNED_BYTE),
        Texture(path2.c_str(), "specular", 1, GL_RED, GL_UNSIGNED_BYTE)
    };

    // Generates Shader object using shaders default.vert and default.frag
    Shader shaderProgram(getShaderPath("default.vert").c_str(),
                         getShaderPath("default.frag").c_str());
    // Store mesh data in vectors for the mesh
    std::vector <Vertex> verts(vertices, vertices + sizeof(vertices) / sizeof(Vertex));
    std::vector <GLuint> ind(indices, indices + sizeof(indices) / sizeof(GLuint));
    std::vector <Texture> tex(textures, textures + sizeof(textures) / sizeof(Texture));
    // Create floor mesh
    Mesh floor(verts, ind, tex);

    // Shader for light cube
    Shader lightShader(getShaderPath("light.vert").c_str(),
                       getShaderPath("light.frag").c_str());
    // Store mesh data in vectors for the mesh
    std::vector <Vertex> lightVerts(lightVertices, lightVertices + sizeof(lightVertices) / sizeof(Vertex));
    std::vector <GLuint> lightInd(lightIndices, lightIndices + sizeof(lightIndices) / sizeof(GLuint));
    // Crate light mesh
    Mesh light(lightVerts, lightInd, tex);

    glm::vec4 lightColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    glm::vec3 lightPos = glm::vec3(0.5f, 0.5f-1, 0.5f);
    glm::mat4 lightModel = glm::mat4(1.0f);
    lightModel = glm::translate(lightModel, lightPos);

    glm::vec3 objectPos = glm::vec3(0.0f, 0.0f-1, 0.0f);
    glm::mat4 objectModel = glm::mat4(1.0f);
    objectModel = glm::translate(objectModel, objectPos);

    float innerCone = 0.95f;
    float outerCone = 0.90f;
    float ambient = 0.20f;

    MeshFactory meshFactory;

    Mesh groundMesh=meshFactory.getGroundFloor(150, 150, 5);

    /*
    lightShader.Activate();
    glUniformMatrix4fv(glGetUniformLocation(lightShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(lightModel));
    glUniform4f(glGetUniformLocation(lightShader.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);

    shaderProgram.Activate();
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE, glm::value_ptr(objectModel));
    glUniform3f(glGetUniformLocation(shaderProgram.ID, "lightPos"), lightPos.x, lightPos.y, lightPos.z);
    glUniform4f(glGetUniformLocation(shaderProgram.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);
*/

    // Enables the Depth Buffer
    glEnable(GL_DEPTH_TEST);

    // Creates camera object
    Camera camera(width, height, glm::vec3(0.0f, 0.0f, 2.0f));

    int selectedScene = 6;

    std::vector<std::string> sceneList;
    sceneList.push_back("blank");
    sceneList.push_back("Lightcube and Floor");
    sceneList.push_back("Bezier test");
    sceneList.push_back("Road tester");
    sceneList.push_back("Road editor");
    sceneList.push_back("Looped spline flat");
    sceneList.push_back("Looped road flat");

    const char* scenes[sceneList.size()];
    for (int iii=0; iii<sceneList.size(); iii++)
    {
        scenes[iii] = sceneList[iii].c_str();
    }

    ////////////////////////////////////////////////////////////////
    /// BEZIER TEST
    ////////////////////////////////////////////////////////////////

    float perpendicularOffset = 0.5;

    int numPoints=3;
    float t=0.0f;
    int curveResolution=250;
    std::vector<glm::vec4> pointTransforms;
    pointTransforms.push_back({-10, 0, -10, 0.0f});
    //pointTransforms.push_back({-10, 0,  10});
    pointTransforms.push_back({ 10, 0, -10, 0.0f});
    pointTransforms.push_back({ 10, 0,  10, 0.0f});

    glm::vec4 leftDerTransform(0.0f);
    glm::vec4 rightDerTransform(0.0f);
    Mesh leftDer = meshFactory.getSphere(0.2f, {0.5f, 0.0f, 0.0f});
    Mesh rightDer = meshFactory.getSphere(0.2f, {0.0f, 0.5f, 0.0f});

    glm::vec4 leftPerTransform(0.0f);
    glm::vec4 rightPerTransform(0.0f);
    Mesh leftPer = meshFactory.getSphere(0.2f, COLORS[RED]);
    Mesh rightPer = meshFactory.getSphere(0.2f, COLORS[GREEN]);

    std::vector<Mesh> points;
    points.push_back(meshFactory.getSphere(1, COLORS[GREEN]));
    //points.push_back(meshFactory.getSphere(1));
    points.push_back(meshFactory.getSphere(1));
    points.push_back(meshFactory.getSphere(1, COLORS[BLUE]));

    Mesh bezierPoint = meshFactory.getSphere(0.5, COLORS[WHITE]);

    bool showCurve=true;
    int maxCurvePoints=250;
    std::vector<glm::vec4> curveTransforms;
    for (int iii = 0; iii < maxCurvePoints; ++iii) {
        curveTransforms.push_back({0, 0, 0, 1});
    }

    std::vector<Mesh> curvePoints;
    for (int iii = 0; iii < maxCurvePoints; ++iii) {
        curvePoints.push_back(meshFactory.getSphere(0.05, {0.9f, 0.1f, 0.9f}));
    }

    ////////////////////////////////////////////////////////////////
    /// ROAD TESTER
    ////////////////////////////////////////////////////////////////

    std::vector<glm::vec4> rightPositions = meshFactory.getTestRight();
    std::vector<glm::vec4> leftPositions = meshFactory.getTestLeft();

    std::vector<Mesh> rightCurve;
    std::vector<Mesh> leftCurve;

    for (int iii = 0; iii < rightPositions.size(); ++iii) {
        rightCurve.push_back(meshFactory.getSphere(0.3f, COLORS[RED]));
        leftCurve.push_back(meshFactory.getSphere(0.3f, COLORS[GREEN]));
    }

    int selectedPart=2;

    std::vector<std::string> partList;
    partList.push_back("Test");
    partList.push_back("Straight");
    partList.push_back("Turn Right");
    partList.push_back("Turn Left");

    const char* parts[partList.size()];
    for (int iii=0; iii<partList.size(); iii++)
    {
        parts[iii] = partList[iii].c_str();
    }

    std::vector<Mesh> allParts;
    allParts.push_back(meshFactory.getPartStraight());
    allParts.push_back(meshFactory.getPartTurnRight());
    allParts.push_back(meshFactory.getPartTurnLeft());

    ////////////////////////////////////////////////////////////////
    /// ROAD EDITOR
    ////////////////////////////////////////////////////////////////

    int cubeResolution = 200;
    glm::vec4 p0({0.0f, 0.0f, 0.0f, 1.0f});
    glm::vec4 p1({0.0f, 0.0f, 0.0f, 1.0f});
    glm::vec4 p2({0.0f, 0.0f, 0.0f, 1.0f});
    glm::vec4 p3({0.0f, 0.0f, 0.0f, 1.0f});

    std::vector<glm::vec4> cubeBez;

    ////////////////////////////////////////////////////////////////
    /// LOOPED SPLINE TRACK FLAT
    ////////////////////////////////////////////////////////////////

    bool useCentripetal=false;

    int splineResolution = 100;
    GLuint maxSplineResolution = 250;
    GLuint knotsnum = 4;
    std::vector<glm::vec4> knotsTransform(knotsnum);
    for (GLuint iii = 0; iii < knotsTransform.size(); ++iii) {
        knotsTransform[iii] = glm::vec4(0.0f);
        knotsTransform[iii].w = 1.0f;

        if (knotsTransform.size() == 4)
        {
            if (iii == 1 || iii == 2)
            {
                knotsTransform[iii].x = 10.0f;
            }
            else
            {
                knotsTransform[iii].x = -10.0f;
            }

            if (iii == 2 || iii == 3)
            {
                knotsTransform[iii].z = 10.0f;
            }
            else
            {
                knotsTransform[iii].z = -10.0f;
            }
        }
    }

    std::vector<Mesh> knots;
    for (GLuint iii = 0; iii < knotsTransform.size(); iii++)
    {
        knots.push_back(meshFactory.getSphere(0.5f, COLORS[WHITE]));
    }

    float splineT=0.0f;
    float splineAlpha=0.5f;
    float splineTangentWidth=1.0f;

    std::vector<std::vector<glm::vec4>> splinesTransform;
    for (GLuint iii = 0; iii < knots.size(); ++iii)
    {
        splinesTransform.push_back(std::vector<glm::vec4>(maxSplineResolution));
    }

    std::vector<std::vector<Mesh>> splines;
    for (GLuint iii = 0; iii < knots.size(); ++iii)
    {
        splines.push_back(std::vector<Mesh>(maxSplineResolution));
        for (int jjj = 0; jjj < splines[iii].size(); ++jjj)
        {
            splines[iii][jjj] = meshFactory.getSphere(0.1f, {0.4f, 0.1f, 0.4f});
        }
    }

    Mesh controlSphere = meshFactory.getSphere(1.0f, COLORS[BLUE]);
    Mesh controlSphereTangent = meshFactory.getSphere(0.8f, COLORS[BLUE]);

    Mesh leftSide = meshFactory.getSphere(0.5f, COLORS[RED]);
    Mesh rightSide = meshFactory.getSphere(0.5f, COLORS[GREEN]);

    ////////////////////////////////////////////////////////////////
    /// LOOPED ROAD FLAT
    ////////////////////////////////////////////////////////////////

    bool onboardCamera = false;
    glm::vec4 oldCoordinates;

    float roadT=0.0f;

    int roadResolution = 100;
    GLuint maxRoadResolution = 250;
    GLuint roadWaypointsNum = 250;

    GLuint posMin = -200;
    GLuint posMax = 200;

    std::vector<Mesh> roadWaypoints(roadWaypointsNum);
    std::vector<glm::vec4> roadPositions;
    for (GLuint iii = 0; iii < roadWaypoints.size(); ++iii)
    {
        roadWaypoints[iii] = meshFactory.getSphere(0.5f, COLORS[WHITE]);

        std::uniform_int_distribution<int> xRange(posMin, posMax);
        std::uniform_int_distribution<int> yRange(0, 0);
        std::uniform_int_distribution<int> zRange(posMin, posMax);

        float x = xRange(rng);
        float y = yRange(rng);
        float z = zRange(rng);

        roadWaypoints[iii].position = glm::vec4(x, y, z, 1.0f);
        roadPositions.push_back(roadWaypoints[iii].position);
    }

    Mesh road = meshFactory.getRoad(roadPositions, roadResolution, 1.0f);
    Mesh carSphere = meshFactory.getSphere(7.5f, COLORS[BLUE]);

/*
    std::vector<Mesh> knots;
    for (GLuint iii = 0; iii < knotsTransform.size(); iii++)
    {
        knots.push_back(meshFactory.getSphere(0.5f, COLORS[WHITE]));
    }

    float splineT=0.0f;
    float splineAlpha=0.5f;
    float splineTangentWidth=1.0f;

    std::vector<std::vector<glm::vec4>> splinesTransform;
    for (GLuint iii = 0; iii < knots.size(); ++iii)
    {
        splinesTransform.push_back(std::vector<glm::vec4>(maxSplineResolution));
    }

    std::vector<std::vector<Mesh>> splines;
    for (GLuint iii = 0; iii < knots.size(); ++iii)
    {
        splines.push_back(std::vector<Mesh>(maxSplineResolution));
        for (int jjj = 0; jjj < splines[iii].size(); ++jjj)
        {
            splines[iii][jjj] = meshFactory.getSphere(0.1f, {0.4f, 0.1f, 0.4f});
        }
    }

    Mesh controlSphere = meshFactory.getSphere(1.0f, COLORS[BLUE]);
    Mesh controlSphereTangent = meshFactory.getSphere(0.8f, COLORS[BLUE]);

    Mesh leftSide = meshFactory.getSphere(0.5f, COLORS[RED]);
    Mesh rightSide = meshFactory.getSphere(0.5f, COLORS[GREEN]);
    */
    ////////////////////////////////////////////////////////////////
    /// LOOP
    ////////////////////////////////////////////////////////////////

    std::vector<GLfloat> FPS;

    // Main while loop
    while (!glfwWindowShouldClose(window))
    {
        FPS.push_back(glfwGetTime());
        while (FPS.back() - FPS.front() >= 1.0f)
        {
            for (GLuint iii=0; iii<FPS.size()-1; iii++)
            {
                FPS[iii] = FPS[iii+1];
            }
            FPS.pop_back();
        }

        //t = glfwGetTime();
        while (t>1.0f)
        {
            t-=1;
        }

        /*
        camera.Position = glm::vec3(meshFactory.quadBezier(pointTransforms, t));
        camera.Position.y += 2;
*/
        ////////////////////////////////////////////////////////////////
        /// Initial rendering
        ////////////////////////////////////////////////////////////////

        // Specify the color of the background
        glClearColor(0.07f, 0.13f, 0.17f, 1.0f);
        // Clean the back buffer and depth buffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        ImGui_ImplGlfwGL3_NewFrame();

        ////////////////////////////////////////////////////////////////
        /// FPS COUNTER
        ////////////////////////////////////////////////////////////////

        ImGuiWindowFlags flags0 = ImGuiWindowFlags_NoResize;
        ImGui::SetNextWindowPos(ImVec2(width-200, 0));
        ImGui::SetNextWindowSize(ImVec2(200, 100));
        ImGui::SetNextWindowBgAlpha(0.5f);
        ImGui::SetNextWindowCollapsed(false);

        ImGui::Begin("FPS", nullptr, flags0);

        ImGui::Text(std::to_string(FPS.size()).c_str());

        ImGui::End();

        ////////////////////////////////////////////////////////////////
        /// Scene selection setup
        ////////////////////////////////////////////////////////////////

        ImGuiWindowFlags flags1 = ImGuiWindowFlags_NoResize;
        ImGui::SetNextWindowPos(ImVec2(0, 0));
        ImGui::SetNextWindowSize(ImVec2(300, sceneList.size()*30));
        ImGui::SetNextWindowBgAlpha(0.5f);
        ImGui::SetNextWindowCollapsed(false);

        ImGui::Begin("Scene Selection", nullptr, flags1);

        ImGui::ListBox("Scene", &selectedScene, scenes, sceneList.size());
        //ImGui::LabelText("Selected", listTest[selectedItem].c_str());
        float sceneSelectorHeight = ImGui::GetWindowHeight();
        ImGui::End();

        ////////////////////////////////////////////////////////////////
        /// Camera handling
        ////////////////////////////////////////////////////////////////

        // Handles camera inputs
        camera.Inputs(window);
        // Updates and exports the camera matrix to the Vertex Shader
        camera.updateMatrix(45.0f, 0.1f, 1000.0f);

        ////////////////////////////////////////////////////////////////
        /// GUI setup
        ////////////////////////////////////////////////////////////////

        ImGui::SetNextWindowPos(ImVec2(0, sceneSelectorHeight));
        ImGui::SetNextWindowSize(ImVec2(300, 500));
        ImGui::SetNextWindowBgAlpha(0.5f);
        ImGui::SetNextWindowCollapsed(false);

        ImGuiWindowFlags flags2 = ImGuiWindowFlags_NoResize;

        switch (selectedScene)
        {
        case 0:
        {
            break;
        }
        case 1:
        {
            ImGui::Begin("Controls", nullptr, flags2);

            ImGui::SliderFloat("Light X", &lightPos.x, -0.5f, 0.5f);
            ImGui::SliderFloat("Light Y", &lightPos.y, -0.5f, 0.5f);
            ImGui::SliderFloat("Light Z", &lightPos.z, -0.5f, 0.5f);
            ImGui::SliderFloat("Inner Cone", &innerCone, 0.1f, 1.0f);
            ImGui::SliderFloat("Outer Cone", &outerCone, 0.1f, 1.0f);
            ImGui::SliderFloat("Ambient", &ambient, 0.0f, 1.0f);

            if (outerCone>innerCone)
            {
                outerCone=innerCone;
            }

            ImGui::End();

            ////////////////////////////////////////////////////////////////
            /// Translation handling
            ////////////////////////////////////////////////////////////////

            glm::mat4 lightModel = glm::mat4(1.0f);
            lightModel = glm::translate(lightModel, lightPos);

            ////////////////////////////////////////////////////////////////
            /// Update shaders
            ////////////////////////////////////////////////////////////////


            lightShader.Activate();
            glUniformMatrix4fv(glGetUniformLocation(lightShader.ID, "model"), 1, GL_FALSE, glm::value_ptr(lightModel));
            glUniform4f(glGetUniformLocation(lightShader.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);

            shaderProgram.Activate();
            glUniform1i(glGetUniformLocation(shaderProgram.ID, "lightToUse"), 2);

            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE, glm::value_ptr(objectModel));
            glUniform3f(glGetUniformLocation(shaderProgram.ID, "lightPos"), lightPos.x, lightPos.y, lightPos.z);
            glUniform4f(glGetUniformLocation(shaderProgram.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);

            glUniform1f(glGetUniformLocation(shaderProgram.ID, "innerCone"), innerCone);
            glUniform1f(glGetUniformLocation(shaderProgram.ID, "outerCone"), outerCone);
            glUniform1f(glGetUniformLocation(shaderProgram.ID, "ambient"), ambient);

            // Draws different meshes
            floor.Draw(shaderProgram, camera);
            light.Draw(lightShader, camera);

            break;
        }
        case 2:
        {
            ImGui::Begin("Controls", nullptr, flags2);

            if (ImGui::Button("Reset positions"))
            {
                pointTransforms[0] = {-10, 0, -10, 1.0f};
                pointTransforms[1] = {-10, 0,  10, 1.0f};
                pointTransforms[2] = { 10, 0, -10, 1.0f};
                //pointTransforms[3] = { 10, 0,  10, 1.0f};
            }

            ImGui::Checkbox("Show curve", &showCurve);
            if (showCurve)
            {
                ImGui::SliderInt("Resolution", &curveResolution, 1, maxCurvePoints);
            }
            ImGui::SliderInt("Points", &numPoints, 2, 3);
            ImGui::SliderFloat("t", &t, 0.0f, 1.0f);

            for (int iii=0; iii<numPoints; iii++)
            {
                std::string label = std::string("Transform ")+std::to_string(iii);
                ImGui::SliderFloat3(label.c_str(), &pointTransforms[iii].x, -100.0f, 100.0f);
            }

            ImGui::End();

            ////////////////////////////////////////////////////////////////
            /// Translation handling
            ////////////////////////////////////////////////////////////////

            glm::mat4 lightModel = glm::mat4(1.0f);
            lightModel = glm::translate(lightModel, lightPos);

            ////////////////////////////////////////////////////////////////
            /// Update shaders
            ////////////////////////////////////////////////////////////////

            shaderProgram.Activate();
            glUniform1i(glGetUniformLocation(shaderProgram.ID, "lightToUse"), 1);

            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE, glm::value_ptr(objectModel));
            glUniform4f(glGetUniformLocation(shaderProgram.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);

            glUniform1f(glGetUniformLocation(shaderProgram.ID, "ambient"), ambient);

            // Draws different meshes
            groundMesh.Draw(shaderProgram, camera);
            glm::mat4 objectModel;

            for (int point=0; point<numPoints; point++)
            {
                objectModel = glm::mat4(1.0f);
                objectModel = glm::translate(objectModel, glm::vec3(pointTransforms[point]));
                glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                                   glm::value_ptr(objectModel));
                //points[point].Draw(shaderProgram, camera);
            }


            objectModel = glm::mat4(1.0f);
            objectModel = glm::translate(objectModel, glm::vec3(meshFactory.quadBezier(pointTransforms, t)));
            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                               glm::value_ptr(objectModel));
            bezierPoint.Draw(shaderProgram, camera);

            //Draw left derivative
            objectModel = glm::mat4(1.0f);
            objectModel = glm::translate(objectModel, glm::vec3(meshFactory.quadBezierDer(pointTransforms, t)));
            objectModel = glm::translate(objectModel, glm::vec3(meshFactory.quadBezier(pointTransforms, t)));
            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                               glm::value_ptr(objectModel));
            leftDer.Draw(shaderProgram, camera);

            //Draw right derivative
            objectModel = glm::mat4(1.0f);
            objectModel = glm::translate(objectModel, -glm::vec3(meshFactory.quadBezierDer(pointTransforms, t)));
            objectModel = glm::translate(objectModel, glm::vec3(meshFactory.quadBezier(pointTransforms, t)));
            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                               glm::value_ptr(objectModel));
            rightDer.Draw(shaderProgram, camera);

            //Draw left perpendicular
            objectModel = glm::mat4(1.0f);
            objectModel = glm::translate(objectModel, glm::vec3(meshFactory.quadBezierPer(pointTransforms, t, perpendicularOffset)));
            objectModel = glm::translate(objectModel, glm::vec3(meshFactory.quadBezier(pointTransforms, t)));
            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                               glm::value_ptr(objectModel));
            leftPer.Draw(shaderProgram, camera);

            //Draw right perpendicual
            objectModel = glm::mat4(1.0f);
            objectModel = glm::translate(objectModel, -glm::vec3(meshFactory.quadBezierPer(pointTransforms, t, perpendicularOffset)));
            objectModel = glm::translate(objectModel, glm::vec3(meshFactory.quadBezier(pointTransforms, t)));
            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                               glm::value_ptr(objectModel));
            rightPer.Draw(shaderProgram, camera);


            curveTransforms=meshFactory.getBezierCurve(pointTransforms, curveResolution);

            if (showCurve)
            {
                float step=1.0f/curveResolution;

                    /*
                for (int iii = 0; iii < curveResolution; ++iii) {
                    curveTransforms[iii] = meshFactory.getBezier(pointTransforms, step*iii);
                }
                        */

                for (int iii = 1; iii < curveResolution; ++iii) {
                    objectModel = glm::mat4(1.0f);
                    objectModel = glm::translate(objectModel, glm::vec3(curveTransforms[iii]));

                    glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                                       glm::value_ptr(objectModel));
                    curvePoints[iii].Draw(shaderProgram, camera);
                }
            }

            break;
        }
        case 3:
        {
            ImGui::Begin("Controls", nullptr, flags2);

            ImGui::ListBox("Part", &selectedPart, parts, partList.size());

            ImGui::End();

            ////////////////////////////////////////////////////////////////
            /// Translation handling
            ////////////////////////////////////////////////////////////////

            glm::mat4 lightModel = glm::mat4(1.0f);
            lightModel = glm::translate(lightModel, lightPos);

            ////////////////////////////////////////////////////////////////
            /// Update shaders
            ////////////////////////////////////////////////////////////////

            shaderProgram.Activate();
            glUniform1i(glGetUniformLocation(shaderProgram.ID, "lightToUse"), 1);

            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE, glm::value_ptr(objectModel));
            glUniform4f(glGetUniformLocation(shaderProgram.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);

            glUniform1f(glGetUniformLocation(shaderProgram.ID, "ambient"), ambient);

            // Draws different meshes
            groundMesh.Draw(shaderProgram, camera);

            glm::mat4 objectModel;

            switch (selectedPart)
            {
            case 0:
            {
                for (GLuint iii = 0; iii < rightPositions.size(); ++iii)
                {
                    objectModel = glm::mat4(1.0f);
                    objectModel = glm::translate(objectModel, glm::vec3(rightPositions[iii]));
                    glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                                       glm::value_ptr(objectModel));
                    rightCurve[iii].Draw(shaderProgram, camera);

                    objectModel = glm::mat4(1.0f);
                    objectModel = glm::translate(objectModel, glm::vec3(leftPositions[iii]));
                    glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                                       glm::value_ptr(objectModel));
                    leftCurve[iii].Draw(shaderProgram, camera);
                }

                break;
            }

            case 1:
            {
                objectModel = glm::mat4(1.0f);

                glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE, glm::value_ptr(objectModel));
                allParts[0].Draw(shaderProgram, camera);

                break;
            }

            case 2:
            {
                objectModel = glm::mat4(1.0f);

                glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE, glm::value_ptr(objectModel));
                allParts[1].Draw(shaderProgram, camera);

                break;
            }

            case 3:
            {
                objectModel = glm::mat4(1.0f);

                glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE, glm::value_ptr(objectModel));
                allParts[2].Draw(shaderProgram, camera);

                break;
            }
            }

            break;
        }
        case 4:
        {
            ImGui::Begin("Controls", nullptr, flags2);

            ImGui::SliderInt("R", &cubeResolution, 2, 250);
            ImGui::SliderFloat3("p0", &p0.x, -2.0f, 2.0f);
            ImGui::SliderFloat3("p1", &p1.x, -2.0f, 2.0f);
            ImGui::SliderFloat3("p2", &p2.x, -2.0f, 2.0f);
            ImGui::SliderFloat3("p3", &p3.x, -2.0f, 2.0f);

            ImGui::End();
            break;
        }
        case 5:
        {
            ImGui::Begin("Controls", nullptr, flags2);

            ImGui::Checkbox("Use centripetal", &useCentripetal);
            ImGui::SliderFloat("a", &splineAlpha, 0.0f, 1.0f);
            ImGui::SliderFloat("t", &splineT, 0.0f, (float)knotsnum);
            ImGui::SliderInt("R", &splineResolution, 2, maxSplineResolution);
            ImGui::SliderFloat("Width", &splineTangentWidth, 0.0f, 10.0f);
            ImGui::SliderFloat3("p0", &knotsTransform[0].x, -20.0f, 20.0f);
            ImGui::SliderFloat3("p1", &knotsTransform[1].x, -20.0f, 20.0f);
            ImGui::SliderFloat3("p2", &knotsTransform[2].x, -20.0f, 20.0f);
            ImGui::SliderFloat3("p3", &knotsTransform[3].x, -20.0f, 20.0f);

            GLfloat mathT2=splineT;
            while (mathT2>=1.0f)
            {
                mathT2 -= 1.0f;
            }
            glm::vec3 debug = glm::vec3(meshFactory.centripetalSpline(p0, p1, p2, p3, mathT2, splineAlpha));
            ImGui::LabelText("x", std::to_string(debug.x).c_str());
            ImGui::LabelText("y", std::to_string(debug.y).c_str());
            ImGui::LabelText("z", std::to_string(debug.z).c_str());

            ImGui::End();

            ////////////////////////////////////////////////////////////////
            /// Translation handling
            ////////////////////////////////////////////////////////////////

            glm::mat4 lightModel = glm::mat4(1.0f);
            lightModel = glm::translate(lightModel, lightPos);

            ////////////////////////////////////////////////////////////////
            /// Update shaders
            ////////////////////////////////////////////////////////////////

            shaderProgram.Activate();
            glUniform1i(glGetUniformLocation(shaderProgram.ID, "lightToUse"), 1);

            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE, glm::value_ptr(objectModel));
            glUniform4f(glGetUniformLocation(shaderProgram.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);

            glUniform1f(glGetUniformLocation(shaderProgram.ID, "ambient"), ambient);

            // Draws different meshes
            groundMesh.Draw(shaderProgram, camera);

            glm::mat4 objectModel;

            for (int iii = 0; iii < knots.size(); ++iii)
            {
                objectModel = glm::mat4(1.0f);
                objectModel = glm::translate(objectModel, glm::vec3(knotsTransform[iii]));
                glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                                   glm::value_ptr(objectModel));
                knots[iii].Draw(shaderProgram, camera);
            }

            //Calculate the spline between the four points
            float stepSize = 1.0f/(splineResolution-1);

            for (int iii = 0; iii < knots.size(); ++iii)
            {
                glm::vec4 p0 = knotsTransform[(0+iii)%knots.size()];
                glm::vec4 p1 = knotsTransform[(1+iii)%knots.size()];
                glm::vec4 p2 = knotsTransform[(2+iii)%knots.size()];
                glm::vec4 p3 = knotsTransform[(3+iii)%knots.size()];

                for (int jjj = 0; jjj < splineResolution; ++jjj)
                {
                    if (useCentripetal)
                    {
                        splinesTransform[iii][jjj] = meshFactory.centripetalSpline(p0, p1, p2, p3, stepSize*(jjj+1), splineAlpha);
                    }
                    else
                    {
                        splinesTransform[iii][jjj] = meshFactory.spline(p0, p1, p2, p3, stepSize*(jjj+1));
                    }
                }
            }

            for (int iii = 0; iii < knots.size(); ++iii)
            {
                for (int jjj = 0; jjj < splineResolution; ++jjj)
                {
                    objectModel = glm::mat4(1.0f);
                    objectModel = glm::translate(objectModel, glm::vec3(splinesTransform[iii][jjj]));
                    glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                                       glm::value_ptr(objectModel));

                    splines[iii][jjj].Draw(shaderProgram, camera);
                }
            }

            glm::vec4 p0 = knotsTransform[(0+(int)splineT)%knots.size()];
            glm::vec4 p1 = knotsTransform[(1+(int)splineT)%knots.size()];
            glm::vec4 p2 = knotsTransform[(2+(int)splineT)%knots.size()];
            glm::vec4 p3 = knotsTransform[(3+(int)splineT)%knots.size()];

            std::cout<<(int)splineT<<std::endl;

            GLfloat mathT=splineT;
            while (mathT>=1.0f)
            {
                mathT -= 1.0f;
            }

            //Draw the controlsphere
            objectModel = glm::mat4(1.0f);
            if (useCentripetal)
            {
                objectModel = glm::translate(objectModel, glm::vec3(meshFactory.centripetalSpline(p0, p1, p2, p3, mathT, splineAlpha)));
            }
            else
            {
                objectModel = glm::translate(objectModel, glm::vec3(meshFactory.spline(p0, p1, p2, p3, mathT)));
            }
            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                               glm::value_ptr(objectModel));
            controlSphere.Draw(shaderProgram, camera);

            //Draw the tangent
            if (useCentripetal)
            {
                objectModel = glm::mat4(1.0f);
                objectModel = glm::translate(objectModel, glm::vec3(meshFactory.centripetalSplineTangent(p0, p1, p2, p3, mathT, splineAlpha)));
                objectModel = glm::translate(objectModel, glm::vec3(meshFactory.centripetalSpline(p0, p1, p2, p3, mathT, splineAlpha)));
                glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                                   glm::value_ptr(objectModel));
                controlSphereTangent.Draw(shaderProgram, camera);
            }
            else
            {
                objectModel = glm::mat4(1.0f);
                objectModel = glm::translate(objectModel, glm::vec3(meshFactory.splineTangent(p0, p1, p2, p3, mathT)));
                objectModel = glm::translate(objectModel, glm::vec3(meshFactory.spline(p0, p1, p2, p3, mathT)));
                glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                                   glm::value_ptr(objectModel));
                //controlSphereTangent.Draw(shaderProgram, camera);
            }

            //Draw the sides
            if (useCentripetal)
            {
            }
            else
            {
                glm::vec4 tangent = meshFactory.splineTangent(p0, p1, p2, p3, mathT);
                glm::vec4 pointTransform = meshFactory.spline(p0, p1, p2, p3, mathT);

                objectModel = glm::mat4(1.0f);
                objectModel = glm::translate(objectModel, glm::vec3(meshFactory.getSidePointLeft(pointTransform, tangent, splineTangentWidth)));
                objectModel = glm::translate(objectModel, glm::vec3(meshFactory.spline(p0, p1, p2, p3, mathT)));
                glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                                   glm::value_ptr(objectModel));
                leftSide.Draw(shaderProgram, camera);

                objectModel = glm::mat4(1.0f);
                objectModel = glm::translate(objectModel, glm::vec3(meshFactory.getSidePointRight(pointTransform, tangent, splineTangentWidth)));
                objectModel = glm::translate(objectModel, glm::vec3(meshFactory.spline(p0, p1, p2, p3, mathT)));
                objectModel = glm::translate(objectModel, glm::vec3(0.0f, -pointTransform.y, 0.0f));
                glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                                   glm::value_ptr(objectModel));
                rightSide.Draw(shaderProgram, camera);
            }

            break;
        }
        case 6:
        {
            ImGui::Begin("Controls", nullptr, flags2);

            ImGui::Checkbox("Onboard camera", &onboardCamera);
            ImGui::SliderFloat("t", &roadT, 0.0f, (float)roadWaypointsNum);

            roadT = glfwGetTime()*5;
            while (roadT>roadWaypointsNum)
            {
                roadT -= roadWaypointsNum;
            }

            int roadInt = roadT;
            GLfloat roadT2=roadT;
            while (roadT2>=1.0f)
            {
                roadT2 -= 1.0f;
            }
            ImGui::End();

            ////////////////////////////////////////////////////////////////
            /// Translation handling
            ////////////////////////////////////////////////////////////////

            glm::mat4 lightModel = glm::mat4(1.0f);
            lightModel = glm::translate(lightModel, lightPos);

            ////////////////////////////////////////////////////////////////
            /// Update shaders
            ////////////////////////////////////////////////////////////////

            shaderProgram.Activate();
            glUniform1i(glGetUniformLocation(shaderProgram.ID, "lightToUse"), 1);

            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE, glm::value_ptr(objectModel));
            glUniform4f(glGetUniformLocation(shaderProgram.ID, "lightColor"), lightColor.x, lightColor.y, lightColor.z, lightColor.w);

            glUniform1f(glGetUniformLocation(shaderProgram.ID, "ambient"), ambient);

            // Draws different meshes
            groundMesh.Draw(shaderProgram, camera);

            glm::mat4 objectModel;

            for (int iii = 0; iii < roadWaypoints.size(); ++iii)
            {
                objectModel = glm::mat4(1.0f);
                objectModel = glm::translate(objectModel, glm::vec3(roadWaypoints[iii].position));
                glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                                   glm::value_ptr(objectModel));
                roadWaypoints[iii].Draw(shaderProgram, camera);
            }

            objectModel = glm::mat4(1.0f);
            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                               glm::value_ptr(objectModel));
            road.Draw(shaderProgram, camera);

            //Draw the car
            glm::vec4 p0 = roadPositions[(0+roadInt)%roadPositions.size()];
            glm::vec4 p1 = roadPositions[(1+roadInt)%roadPositions.size()];
            glm::vec4 p2 = roadPositions[(2+roadInt)%roadPositions.size()];
            glm::vec4 p3 = roadPositions[(3+roadInt)%roadPositions.size()];

            objectModel = glm::mat4(1.0f);
            objectModel = glm::translate(objectModel, glm::vec3(meshFactory.spline(p0, p1, p2, p3, roadT2)));
            glUniformMatrix4fv(glGetUniformLocation(shaderProgram.ID, "model"), 1, GL_FALSE,
                               glm::value_ptr(objectModel));
            carSphere.Draw(shaderProgram, camera);

            if (onboardCamera)
            {
                camera.Position = glm::vec3(meshFactory.spline(p0, p1, p2, p3, roadT2));
                camera.Position.y += 1.0f;
            }


            break;
        }
        }

        ////////////////////////////////////////////////////////////////
        /// Final rendering
        ////////////////////////////////////////////////////////////////

        ImGui::Render();
        ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());

        // Swap the back buffer with the front buffer
        glfwSwapBuffers(window);
        // Take care of all GLFW events
        glfwPollEvents();
    }



    // Delete all the objects we've created
    shaderProgram.Delete();
    lightShader.Delete();
    // Delete window before ending the program
    glfwDestroyWindow(window);
    // Terminate GLFW before ending the program
    glfwTerminate();
    return 0;
}
