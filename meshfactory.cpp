#include "meshfactory.h"

MeshFactory::MeshFactory()
{
    partResolution=100;
}

Mesh MeshFactory::getRoad(std::vector<glm::vec4> waypoints, GLuint resolution, GLfloat width)
{
    GLuint steps = waypoints.size()*resolution;

    std::vector<GLfloat> tList;

    std::vector<glm::vec4> leftCurve;
    std::vector<glm::vec4> rightCurve;

    for (int step = 0; step < steps; ++step)
    {
        GLfloat fWaypoints = waypoints.size();
        GLfloat fSteps = steps;
        GLfloat fStep = step;

        GLfloat t = fWaypoints/fSteps*fStep;

        GLuint tInt = t;
        GLfloat tNormal = t;
        while (tNormal >= 1.0f)
        {
            tNormal -= 1.0f;
        }

        glm::vec4 p0 = waypoints[(0+tInt)%waypoints.size()];
        glm::vec4 p1 = waypoints[(1+tInt)%waypoints.size()];
        glm::vec4 p2 = waypoints[(2+tInt)%waypoints.size()];
        glm::vec4 p3 = waypoints[(3+tInt)%waypoints.size()];

        glm::vec4 position = spline(p0, p1, p2, p3, tNormal);
        glm::vec4 tangent = splineTangent(p0, p1, p2, p3, tNormal);
        glm::vec4 left = getSidePointLeft(position, tangent, width)+position;
        glm::vec4 right = getSidePointRight(position, tangent, width)+position;

        leftCurve.push_back(left);
        rightCurve.push_back(right);
    }

    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;

    glm::vec3 leftColor(1.0f, 0.0f, 0.0f);
    glm::vec3 rightColor(0.0f, 1.0f, 0.0f);

    for (GLuint iii=0; iii<leftCurve.size(); iii++)
    {
        indices.push_back(vertices.size());
        indices.push_back(vertices.size()+1);
        indices.push_back(vertices.size()+2);
        indices.push_back(vertices.size());
        indices.push_back(vertices.size()+2);
        indices.push_back(vertices.size()+3);

        glm::vec3 position(0.0f, 0.0f, 0.0f);
        glm::vec3 normal(0.0f, 1.0f, 0.0f);
        glm::vec2 texPosition(0.0f, 0.0f);

        position=leftCurve[iii];
        texPosition.x = 0.0f;
        texPosition.y = 0.0f;
        vertices.push_back({position, normal, leftColor, texPosition});

        position=rightCurve[iii];
        texPosition.x = 1.0f;
        vertices.push_back({position, normal, rightColor, texPosition});

        position=rightCurve[(iii+1)%rightCurve.size()];
        texPosition.y = 1.0f;
        vertices.push_back({position, normal, rightColor, texPosition});

        position=leftCurve[(iii+1)%leftCurve.size()];
        texPosition.x = 0.0f;
        vertices.push_back({position, normal, leftColor, texPosition});
    }

    std::string texturePath="../GeometryMorphing/resources/textures/white.png";

    Texture textures[]
    {
        Texture(texturePath.c_str(), "diffuse", 0, GL_RGBA, GL_UNSIGNED_BYTE)
    };
    std::vector<Texture> tex(textures, textures + sizeof(textures) / sizeof(Texture));

    std::cout<<"Final vertex count for ROAD: "<<vertices.size()<<std::endl;

    return Mesh(vertices, indices, tex);
}

glm::vec4 MeshFactory::getSidePointLeft(glm::vec4 point, glm::vec4 tangent, GLfloat n)
{
    glm::vec4 side(point);

    side.x = tangent.z*n;
    side.z = -tangent.x*n;

    return side;
}

glm::vec4 MeshFactory::getSidePointRight(glm::vec4 point, glm::vec4 tangent, GLfloat n)
{
    glm::vec4 side(point);

    side.x = -tangent.z*n;
    side.z = tangent.x*n;

    return side;
}

GLfloat GetT( float t, float alpha, glm::vec4 p0, glm::vec4 p1)
{
    using namespace glm;

    alpha *= 0.5f;

    GLfloat x = pow(p1.x - p0.x, 2);
    GLfloat y = pow(p1.y - p0.y, 2);
    GLfloat z = pow(p1.z - p0.z, 2);
    GLfloat root = pow(x+y+z, alpha);

    return root + t;
}

glm::vec4 MeshFactory::centripetalSplineTangent(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2, glm::vec4 p3, GLfloat t, GLfloat a)
{
    //http://denkovacs.com/2016/02/catmull-rom-spline-derivatives/

    /*

    % Evaluate CR-Spline Derivatives
    A1p = (P1 - P0)./(t1 - t0);
    A2p = (P2 - P1)./(t2 - t1);
    A3p = (P3 - P2)./(t3 - t2);

    B1p = (A2 - A1) ./ (t2 - t0) + (t2 - t)./(t2 - t0) .* A1p + (t - t0)./(t2 - t0) .* A2p;
    B2p = (A3 - A2) ./ (t3 - t1) + (t3 - t)./(t3 - t1) .* A2p + (t - t1)./(t3 - t1) .* A3p;

    Cp = (B2 - B1) ./ (t2 - t1) + (t2 - t)./(t2 - t1) .* B1p + (t - t1)./(t2 - t1) .* B2p;

*/

    GLfloat t0 = 0.0f;
    GLfloat t1 = GetT( t0, a, p0, p1 );
    GLfloat t2 = GetT( t1, a, p1, p2 );
    GLfloat t3 = GetT( t2, a, p2, p3 );
    t = lerpf(t1, t2, t);

    GLfloat xA1 = 1.0f / (t1-t0) * (p1.x-p0.x);
    GLfloat xA2 = 1.0f / (t2-t1) * (p2.x-p1.x);
    GLfloat xA3 = 1.0f / (t3-t2) * (p3.x-p2.x);
    GLfloat xB1 = 1.0f / (t2-t0) * (xA2-xA1) + (t2-t) / (t2-t0)*xA1 + (t-t0) / (t2-t0)*xA2;
    GLfloat xB2 = 1.0f / (t3-t1) * (xA3-xA2) + (t3-t) / (t3-t1)*xA2 + (t-t1) / (t3-t1)*xA3;
    GLfloat xC  = 1.0f / (t2-t1) * (xB2-xB1) + (t2-t) / (t2-t1)*xB1 + (t-t1) / (t2-t1)*xB2;

    GLfloat yA1 = 1.0f / (t1-t0) * (p1.y-p0.y);
    GLfloat yA2 = 1.0f / (t2-t1) * (p2.y-p1.y);
    GLfloat yA3 = 1.0f / (t3-t2) * (p3.y-p2.y);
    GLfloat yB1 = 1.0f / (t2-t0) * (xA2-yA1) + (t2-t) / (t2-t0)*yA1 + (t-t0) / (t2-t0)*yA2;
    GLfloat yB2 = 1.0f / (t3-t1) * (xA3-yA2) + (t3-t) / (t3-t1)*yA2 + (t-t1) / (t3-t1)*yA3;
    GLfloat yC  = 1.0f / (t2-t1) * (xB2-yB1) + (t2-t) / (t2-t1)*yB1 + (t-t1) / (t2-t1)*yB2;

    GLfloat zA1 = 1.0f / (t1-t0) * (p1.z-p0.z);
    GLfloat zA2 = 1.0f / (t2-t1) * (p2.z-p1.z);
    GLfloat zA3 = 1.0f / (t3-t2) * (p3.z-p2.z);
    GLfloat zB1 = 1.0f / (t2-t0) * (zA2-zA1) + (t2-t) / (t2-t0)*zA1 + (t-t0) / (t2-t0)*zA2;
    GLfloat zB2 = 1.0f / (t3-t1) * (zA3-zA2) + (t3-t) / (t3-t1)*zA2 + (t-t1) / (t3-t1)*zA3;
    GLfloat zC  = 1.0f / (t2-t1) * (zB2-zB1) + (t2-t) / (t2-t1)*zB1 + (t-t1) / (t2-t1)*zB2;

    xC  = 1.0f/(t2-t1)*(xB2-xB1) + (t2-t)/(t2-t1)*xB1*t + (t-t1)/(t2-t1)*xB2*t;
    yC  = 1.0f/(t2-t1)*(xB2-yB1) + (t2-t)/(t2-t1)*yB1*t + (t-t1)/(t2-t1)*yB2*t;
    zC  = 1.0f/(t2-t1)*(zB2-zB1) + (t2-t)/(t2-t1)*zB1*t + (t-t1)/(t2-t1)*zB2*t;

    yC  = lerpf(p1.y, p2.y, t);

    return glm::normalize(glm::vec4(xC, yC, zC, 1.0f));
}

glm::vec4 MeshFactory::splineTangent(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2, glm::vec4 p3, GLfloat t)
{
    GLfloat tt = t*t;
    GLfloat half = 0.5f;

    GLfloat ax=3*(p3.x-3*p2.x+3*p1.x-p0.x)*tt;
    GLfloat bx=2*(-p3.x+4*p2.x-5*p1.x+2*p0.x)*t;
    GLfloat cx=p2.x-p0.x;

    GLfloat ay=3*(p3.y-3*p2.y+3*p1.y-p0.y)*tt;
    GLfloat by=2*(-p3.y+4*p2.y-5*p1.y+2*p0.y)*t;
    GLfloat cy=p2.y-p0.y;

    GLfloat az=3*( p3.z-3*p2.z+3*p1.z-p0.z)*tt;
    GLfloat bz=2*(-p3.z+4*p2.z-5*p1.z+2*p0.z)*t;
    GLfloat cz=p2.z-p0.z;

    GLfloat x=half*(ax+bx+cx);
    GLfloat y=half*(ay+by+cy);
    GLfloat z=half*(az+bz+cz);

    return glm::normalize(glm::vec4(x, y, z, 1.0f));
}

glm::vec4 MeshFactory::centripetalSpline(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2, glm::vec4 p3, GLfloat t, GLfloat a)
{
    GLfloat t0 = 0.0f;
    GLfloat t1 = GetT( t0, a, p0, p1 );
    GLfloat t2 = GetT( t1, a, p1, p2 );
    GLfloat t3 = GetT( t2, a, p2, p3 );
    t = lerpf(t1, t2, t);

    GLfloat xA1 = ( t1-t )/( t1-t0 )*p0.x + ( t-t0 )/( t1-t0 )*p1.x;
    GLfloat xA2 = ( t2-t )/( t2-t1 )*p1.x + ( t-t1 )/( t2-t1 )*p2.x;
    GLfloat xA3 = ( t3-t )/( t3-t2 )*p2.x + ( t-t2 )/( t3-t2 )*p3.x;
    GLfloat xB1 = ( t2-t )/( t2-t0 )*xA1  + ( t-t0 )/( t2-t0 )*xA2;
    GLfloat xB2 = ( t3-t )/( t3-t1 )*xA2  + ( t-t1 )/( t3-t1 )*xA3;
    GLfloat xC  = ( t2-t )/( t2-t1 )*xB1  + ( t-t1 )/( t2-t1 )*xB2;

    GLfloat yA1 = ( t1-t )/( t1-t0 )*p0.y + ( t-t0 )/( t1-t0 )*p1.y;
    GLfloat yA2 = ( t2-t )/( t2-t1 )*p1.y + ( t-t1 )/( t2-t1 )*p2.y;
    GLfloat yA3 = ( t3-t )/( t3-t2 )*p2.y + ( t-t2 )/( t3-t2 )*p3.y;
    GLfloat yB1 = ( t2-t )/( t2-t0 )*yA1  + ( t-t0 )/( t2-t0 )*yA2;
    GLfloat yB2 = ( t3-t )/( t3-t1 )*yA2  + ( t-t1 )/( t3-t1 )*yA3;
    GLfloat yC  = ( t2-t )/( t2-t1 )*yB1  + ( t-t1 )/( t2-t1 )*yB2;

    GLfloat zA1 = ( t1-t )/( t1-t0 )*p0.z + ( t-t0 )/( t1-t0 )*p1.z;
    GLfloat zA2 = ( t2-t )/( t2-t1 )*p1.z + ( t-t1 )/( t2-t1 )*p2.z;
    GLfloat zA3 = ( t3-t )/( t3-t2 )*p2.z + ( t-t2 )/( t3-t2 )*p3.z;
    GLfloat zB1 = ( t2-t )/( t2-t0 )*zA1  + ( t-t0 )/( t2-t0 )*zA2;
    GLfloat zB2 = ( t3-t )/( t3-t1 )*zA2  + ( t-t1 )/( t3-t1 )*zA3;
    GLfloat zC  = ( t2-t )/( t2-t1 )*zB1  + ( t-t1 )/( t2-t1 )*zB2;

    return glm::vec4(xC, yC, zC, 1.0f);
}

glm::vec4 MeshFactory::spline(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2, glm::vec4 p3, GLfloat t)
{
    //https://www.mvps.org/directx/articles/catmull/

    using namespace glm;

    GLfloat half = 0.5f;

    GLfloat tt = pow(t, 2);
    GLfloat ttt = pow(t, 3);

    GLfloat ax=2*p1.x;
    GLfloat bx=(-p0.x+p2.x)*t;
    GLfloat cx=(2*p0.x - 5*p1.x + 4*p2.x - p3.x) * tt;
    GLfloat dx=(-p0.x + 3*p1.x - 3*p2.x + p3.x) * ttt;

    GLfloat ay=2*p1.y;
    GLfloat by=(-p0.y+p2.y)*t;
    GLfloat cy=(2*p0.y - 5*p1.y + 4*p2.y - p3.y) * tt;
    GLfloat dy=(-p0.y + 3*p1.y - 3*p2.y + p3.y) * ttt;

    GLfloat az=2*p1.z;
    GLfloat bz=(-p0.z+p2.z)*t;
    GLfloat cz=(2*p0.z - 5*p1.z + 4*p2.z - p3.z) * tt;
    GLfloat dz=(-p0.z + 3*p1.z - 3*p2.z + p3.z) * ttt;

/*
    vec4 a(1.0f, t, pow(t, 2), pow(t, 3));
    mat4 b( 0.0f,  2.0f,  0.0f,  0.0f,
           -1.0f,  0.0f,  1.0f,  0.0f,
            2.0f, -5.0f,  4.0f, -1.0f,
           -1.0f,  3.0f, -3.0f,  1.0f);

    vec4 cx(p0.x, p1.x, p2.x, p3.x);
    vec4 cy(p0.y, p1.y, p2.y, p3.y);
    vec4 cz(p0.z, p1.z, p2.z, p3.z);

    GLfloat x = half*a*b*cx;
    GLfloat y = half*a*b*cy;
    GLfloat z = half*a*b*cz;
*/

    return glm::vec4(half*(ax+bx+cx+dx),
                     half*(ay+by+cy+dy),
                     half*(az+bz+cz+dz), 1.0f);
}

glm::vec4 MeshFactory::lerpV4(glm::vec4 p0, glm::vec4 p1, float t)
{
    // (1 - t) * v0 + t * v1
    return glm::vec4((1.0f - t) * p0.x + t * p1.x,
                     (1.0f - t) * p0.y + t * p1.y,
                     (1.0f - t) * p0.z + t * p1.z,
                     1.0f);
}

float MeshFactory::lerpf(GLfloat x, GLfloat y, GLfloat t)
{
    return (1.0f-t)*x+t*y;
}

glm::vec4 MeshFactory::getBezier3(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2, GLfloat t)
{
    using namespace glm;

    float bx = pow((1-t), 2)*p0.x+2*(1-t)*t*p1.x+pow(t, 2)*p2.x;
    float by = pow((1-t), 2)*p0.y+2*(1-t)*t*p1.y+pow(t, 2)*p2.y;
    float bz = pow((1-t), 2)*p0.z+2*(1-t)*t*p1.z+pow(t, 2)*p2.z;

    return glm::vec4(bx, by, bz, 1.0f);
}

glm::vec4 MeshFactory::getBezier4(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2, glm::vec4 p3, GLfloat t)
{
    using namespace glm;
    //https://en.wikipedia.org/wiki/B%C3%A9zier_curve#Cubic_B%C3%A9zier_curves
    float bx = pow((1-t), 3)*p0.x + 3*pow((1-t), 2)*t*p1.x + 3*(1-t)*pow(t, 2)*p2.x + pow(t, 3)*p3.x;
    float by = pow((1-t), 3)*p0.y + 3*pow((1-t), 2)*t*p1.y + 3*(1-t)*pow(t, 2)*p2.y + pow(t, 3)*p3.y;
    float bz = pow((1-t), 3)*p0.z + 3*pow((1-t), 2)*t*p1.z + 3*(1-t)*pow(t, 2)*p2.z + pow(t, 3)*p3.z;

    return glm::vec4(bx, by, bz, 1.0f);
}

glm::vec4 MeshFactory::getBezierN(std::vector<glm::vec4> points, GLfloat t)
{
    while (points.size()>1)
    {
        for (GLuint iii=0; iii<points.size()-1; iii++)
        {
            glm::vec4 p0 = points[iii];
            glm::vec4 p1 = points[iii+1];

            points[iii] = lerpV4(p0, p1, t);
        }

        points.pop_back();
    }

    return points[0];
}

glm::vec4 MeshFactory::getBezierV2(std::vector<glm::vec4> points, float t)
{
    if (points.size() == 4)
        return getBezier4(points[0], points[1], points[2], points[3], t);

    if (points.size() == 3)
        return getBezier3(points[0], points[1], points[2], t);

    return getBezierN(points, t);
}

glm::vec4 MeshFactory::getBezier(std::vector<glm::vec4> points, float t)
{
    while (points.size()>1)
    {
        std::vector<glm::vec4> bezierPoints;

        for (GLuint iii=0; iii<points.size()-1; iii++)
        {
            glm::vec4 p0 = points[iii];
            glm::vec4 p1 = points[iii+1];

            points[iii] = lerpV4(p0, p1, t);
        }

        points.pop_back();
    }

    return points[0];
}

glm::vec4 MeshFactory::getBezier(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, float t)
{
    std::vector<glm::vec4> points;
    points.push_back(glm::vec4(p0, 1.0f));
    points.push_back(glm::vec4(p1, 1.0f));
    points.push_back(glm::vec4(p2, 1.0f));

    return getBezier(points, t);
}

std::vector<glm::vec4> MeshFactory::getBezierCurve(std::vector<glm::vec4> points, GLuint steps)
{
    float stepSize=1.0f/(steps-1);

    std::vector<glm::vec4> curve;

    for (int iii = 0; iii < steps; ++iii) {
        curve.push_back(getBezier(points, stepSize*iii));
        std::cout<<curve[iii-1].x<<std::endl;
    }

    std::cout<<"Resolution: "<<partResolution<<", curvesize: "<<curve.size()<<std::endl;
    return curve;
}

glm::vec4 MeshFactory::quadBezier(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, GLfloat t)
{
    float bx = glm::pow((1-t), 2)*p0.x+2*(1-t)*t*p1.x+pow(t, 2)*p2.x;
    float by = glm::pow((1-t), 2)*p0.y+2*(1-t)*t*p1.y+pow(t, 2)*p2.y;
    float bz = glm::pow((1-t), 2)*p0.z+2*(1-t)*t*p1.z+pow(t, 2)*p2.z;

    return glm::vec4(bx, by, bz, 1.0f);
}

glm::vec4 MeshFactory::quadBezier(std::vector<glm::vec4> points, GLfloat t)
{
    return quadBezier(points[0], points[1], points[2], t);
}

glm::vec4 MeshFactory::quadBezierDer(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, GLfloat t)
{
    float bx = 2*(1-t)*(p1.x-p0.x)+2*t*(p2.x-p1.x);
    float by = 2*(1-t)*(p1.y-p0.y)+2*t*(p2.y-p1.y);
    float bz = 2*(1-t)*(p1.z-p0.z)+2*t*(p2.z-p1.z);

    return glm::normalize(glm::vec4(bx, by, bz, 1.0f));
}

glm::vec4 MeshFactory::quadBezierDer(std::vector<glm::vec4> points, GLfloat t)
{
    return quadBezierDer(points[0], points[1], points[2], t);
}

glm::vec4 MeshFactory::quadBezierPer(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, GLfloat t, GLfloat n)
{
    glm::vec4 d(quadBezierDer(p0, p1, p2, t));

    glm::vec4 perpendicular(1.0f);
    float x =  d.z/glm::sqrt(glm::pow(d.x, 2)+glm::pow(d.z, 2));
    float z = -d.x/glm::sqrt(glm::pow(d.x, 2)+glm::pow(d.z, 2));

    perpendicular.x = x*n;
    perpendicular.y = d.y;
    perpendicular.z = z*n;

    return perpendicular;
}

glm::vec4 MeshFactory::quadBezierPer(std::vector<glm::vec4> points, GLfloat t, GLfloat n)
{
    return quadBezierPer(points[0], points[1], points[2], t, n);
}

std::vector<glm::vec4> MeshFactory::quadBezierCurve(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, GLuint steps)
{

}

std::vector<glm::vec4> MeshFactory::quadBezierCurve(std::vector<glm::vec4> points, GLuint steps)
{
    if (points.size() == 3)
    {
        return quadBezierCurve(points[0], points[1], points[2], steps);
    }

    throw 1;
}

glm::vec4 MeshFactory::cubeBez(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, GLfloat t)
{
    using namespace glm;
    //https://en.wikipedia.org/wiki/B%C3%A9zier_curve#Cubic_B%C3%A9zier_curves
    glm::vec4 b(1.0f);
    b.x = pow((1-t), 3)*p0.x + 3*pow((1-t), 2)*t*p1.x + 3*(1-t)*pow(t, 2)*p2.x + pow(t, 3)*p3.x;
    b.y = pow((1-t), 3)*p0.y + 3*pow((1-t), 2)*t*p1.y + 3*(1-t)*pow(t, 2)*p2.y + pow(t, 3)*p3.y;
    b.z = pow((1-t), 3)*p0.z + 3*pow((1-t), 2)*t*p1.z + 3*(1-t)*pow(t, 2)*p2.z + pow(t, 3)*p3.z;

    return b;
}

glm::vec4 MeshFactory::cubeBezDer(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, GLfloat t)
{
    using namespace glm;
    //https://en.wikipedia.org/wiki/B%C3%A9zier_curve#Cubic_B%C3%A9zier_curves
    glm::vec4 b(1.0f);
    b.x = 3*pow((1-t), 2)*(p1.x-p0.x) + 6*(1-t)*t*(p2.x-p1.x) + 3*pow(t, 2)*(p3.x-p2.x);
    b.y = 3*pow((1-t), 2)*(p1.y-p0.y) + 6*(1-t)*t*(p2.y-p1.y) + 3*pow(t, 2)*(p3.y-p2.y);
    b.z = 3*pow((1-t), 2)*(p1.z-p0.z) + 6*(1-t)*t*(p2.z-p1.z) + 3*pow(t, 2)*(p3.z-p2.z);

    return b;
}

glm::vec4 MeshFactory::cubeBezPer(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, GLfloat t, GLfloat o)
{

}

Mesh MeshFactory::getGroundFloor(int length,
                                 int width,
                                 float size,
                                 glm::vec3 c1,
                                 glm::vec3 c2)
{
    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;

    for (int x=0; x<length; x++)
    {
        for (int y=0; y<width; y++)
        {
            indices.push_back(vertices.size());
            indices.push_back(vertices.size()+1);
            indices.push_back(vertices.size()+2);
            indices.push_back(vertices.size());
            indices.push_back(vertices.size()+2);
            indices.push_back(vertices.size()+3);

            float xOffset = (length*size)/2;
            float yOffset = (width*size)/2;

            float x1 = x*size-xOffset;
            float x2 = x*size+size-yOffset;

            float z1 = y*size-xOffset;
            float z2 = y*size+size-yOffset;

            bool colorSelect = x%2;

            if (y%2)
            {
                colorSelect = !colorSelect;
            }

            glm::vec3 color;

            if (colorSelect)
            {
                color=c1;
            }
            else
            {
                color=c2;
            }

            glm::vec3 position(x1, 0, z1);
            glm::vec2 texPosition(0, 0);
            glm::vec3 normal(0, 1, 0);

            vertices.push_back({position, normal, color, texPosition});

            position.x = x2;
            texPosition.x = 1.0f;
            vertices.push_back({position, normal, color, texPosition});

            position.z = z2;
            texPosition.y = 1.0f;
            vertices.push_back({position, normal, color, texPosition});

            position.x = x1;
            texPosition.x = 0.0f;
            vertices.push_back({position, normal, color, texPosition});
        }
    }

    std::string texturePath="../GeometryMorphing/resources/textures/white.png";

    Texture textures[]
    {
        Texture(texturePath.c_str(), "diffuse", 0, GL_RGBA, GL_UNSIGNED_BYTE)
    };
    std::vector<Texture> tex(textures, textures + sizeof(textures) / sizeof(Texture));

    return Mesh(vertices, indices, tex);
}

Mesh MeshFactory::getSphere(float radius, glm::vec3 c)
{
    //http://www.songho.ca/opengl/gl_sphere.htmls
    //http://www.songho.ca/opengl/gl_cylinder.html

    unsigned int stackCount=15;
    unsigned int sectorCount=stackCount;

    std::vector<float> vertices;
    std::vector<float> normals;
    std::vector<float> texCoords;

    std::vector<Vertex> vertices3d;

    float PI = glm::pi<float>();

    float x, y, z, xy;                              // vertex position
    float nx, ny, nz, lengthInv = 1.0f / radius;    // vertex normal
    float s, t;                                     // vertex texCoord

    float sectorStep = 2 * PI / sectorCount;
    float stackStep = PI / stackCount;
    float sectorAngle, stackAngle;

    for (int i = 0; i <= stackCount; ++i)
    {
        stackAngle = PI / 2 - i * stackStep;        // starting from pi/2 to -pi/2
        xy = radius * cosf(stackAngle);             // r * cos(u)
        y = radius * sinf(stackAngle);              // r * sin(u)

        // add (sectorCount+1) vertices per stack
        // the first and last vertices have same position and normal, but different tex coords
        for (int j = 0; j <= sectorCount; ++j)
        {
            sectorAngle = j * sectorStep;           // starting from 0 to 2pi

            // vertex position (x, y, z)
            x = xy * cosf(sectorAngle);             // r * cos(u) * cos(v)
            z = xy * sinf(sectorAngle);             // r * cos(u) * sin(v)
            vertices.push_back(x);
            vertices.push_back(y);
            vertices.push_back(z);

            // normalized vertex normal (nx, ny, nz)
            nx = x * lengthInv;
            ny = y * lengthInv;
            nz = z * lengthInv;
            normals.push_back(nx);
            normals.push_back(ny);
            normals.push_back(nz);

            // vertex tex coord (s, t) range between [0, 1]
            s = (float)j / sectorCount;
            t = (float)i / stackCount;
            texCoords.push_back(s);
            texCoords.push_back(t);

            glm::vec3 position(x, y, z);
            glm::vec3 normal(nx, ny, nz);
            glm::vec2 texCoord(s, t);

            Vertex newVertex;
            newVertex.position = position;
            newVertex.normal = normal;
            newVertex.color = c;
            newVertex.texUV = texCoord;

            vertices3d.push_back(newVertex);
        }
    }

    std::vector<GLuint> indices;
    int k1, k2;
    for(int i = 0; i < stackCount; ++i)
    {
        k1 = i * (sectorCount + 1);     // beginning of current stack
        k2 = k1 + sectorCount + 1;      // beginning of next stack

        for(int j = 0; j < sectorCount; ++j, ++k1, ++k2)
        {
            // 2 triangles per sector excluding first and last stacks
            // k1 => k2 => k1+1
            if(i != 0)
            {
                indices.push_back(k1);
                indices.push_back(k2);
                indices.push_back(k1 + 1);
            }

            // k1+1 => k2 => k2+1
            if(i != (stackCount-1))
            {
                indices.push_back(k1 + 1);
                indices.push_back(k2);
                indices.push_back(k2 + 1);
            }
        }
    }

    std::string texturePath="../GeometryMorphing/resources/textures/white.png";
    Texture textures[]
    {
        Texture(texturePath.c_str(), "diffuse", 0, GL_RGBA, GL_UNSIGNED_BYTE)
    };
    std::vector<Texture> tex(textures, textures + sizeof(textures) / sizeof(Texture));

    return Mesh(vertices3d, indices, tex);
}

Mesh MeshFactory::getPartStraight(float width)
{
    std::vector<glm::vec4> waypoints;
    waypoints.push_back({0.0f, 0.0f, -0.5f, 1.0f});
    waypoints.push_back({0.0f, 0.0f,  0.0f, 1.0f});
    waypoints.push_back({0.0f, 0.0f,  0.5f, 1.0f});

    //Create two curves, one for each side of the roadpiece
    glm::vec3 translationLeft({-width, 0.0f, 0.0f});
    glm::mat4 translateLeft = glm::translate(glm::mat4(1.0f), translationLeft);

    std::vector<glm::vec4> leftWaypoints;
    leftWaypoints.push_back(translateLeft*waypoints[0]);
    leftWaypoints.push_back(translateLeft*waypoints[1]);
    leftWaypoints.push_back(translateLeft*waypoints[2]);

    glm::vec3 translationRight({width, 0.0f, 0.0f});
    glm::mat4 translateRight = glm::translate(glm::mat4(1.0f), translationRight);

    std::vector<glm::vec4> rightWaypoints;
    rightWaypoints.push_back(translateRight*waypoints[0]);
    rightWaypoints.push_back(translateRight*waypoints[1]);
    rightWaypoints.push_back(translateRight*waypoints[2]);

    std::vector<glm::vec4> leftCurve = getBezierCurve(leftWaypoints, partResolution);
    std::vector<glm::vec4> rightCurve = getBezierCurve(rightWaypoints, partResolution);

    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;

    for (GLuint iii = 0; iii < leftCurve.size()-1; ++iii)
    {
        indices.push_back(vertices.size());
        indices.push_back(vertices.size()+1);
        indices.push_back(vertices.size()+2);
        indices.push_back(vertices.size());
        indices.push_back(vertices.size()+2);
        indices.push_back(vertices.size()+3);

        glm::vec3 normal(0.0f, 1.0f, 0.0f);
        glm::vec3 color(1.0f, 1.0f, 1.0f);
        color.x = 0.3;
        color.y = 0.3;
        color.z = 0.3;

        glm::vec3 position(0.0f, 0.0f, 0.0f);
        glm::vec2 texPosition(0.0f, 0.0f);

        position=leftCurve[iii];
        texPosition.x = 0.0f;
        texPosition.y = 0.0f;
        vertices.push_back({position, normal, color, texPosition});

        position=rightCurve[iii];
        texPosition.x = 1.0f;
        vertices.push_back({position, normal, color, texPosition});

        position=rightCurve[iii+1];
        texPosition.y = 1.0f;
        vertices.push_back({position, normal, color, texPosition});

        position=leftCurve[iii+1];
        texPosition.x = 0.0f;
        vertices.push_back({position, normal, color, texPosition});
    }

    std::string texturePath="../GeometryMorphing/resources/textures/white.png";

    Texture textures[]
    {
        Texture(texturePath.c_str(), "diffuse", 0, GL_RGBA, GL_UNSIGNED_BYTE)
    };
    std::vector<Texture> tex(textures, textures + sizeof(textures) / sizeof(Texture));

    std::cout<<"Final vertex count: "<<vertices.size()<<std::endl;

    return Mesh(vertices, indices, tex);
}

Mesh MeshFactory::getPartTurnRight(float width)
{
    glm::vec4 s(0.0f, 0.0f, -1.0f, 1.0f);
    glm::vec4 w(0.0f, 0.0f,  0.0f, 1.0f);
    glm::vec4 e(1.0f, 0.0f,  0.0f, 1.0f);

    std::vector<glm::vec4> waypoints;
    waypoints.push_back(s);
    waypoints.push_back(w);
    waypoints.push_back(e);

    std::vector<glm::vec4> leftCurve;
    std::vector<glm::vec4> rightCurve;

    glm::vec4 lc =  quadBezierPer(s, w, e, 0.0f, width);
    leftCurve.push_back(lc);

    glm::vec4 rc = -quadBezierPer(s, w, e, 0.0f, width);
    rightCurve.push_back(rc);

    float stepSize=1.0f/(partResolution-1);
    for (int iii = 0; iii < partResolution-1; ++iii)
    {
        float t = stepSize*(iii+1);

        glm::vec4 b = getBezier(waypoints, t);

        lc =  quadBezierPer(s, w, e, t, width);
        lc.x += b.x;
        lc.y += b.y;
        lc.z += b.z;
        leftCurve.push_back(lc);

        rc = -quadBezierPer(s, w, e, t, width);
        rc.x += b.x;
        rc.y += b.y;
        rc.z += b.z;
        rightCurve.push_back(rc);
    }

    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;

    for (GLuint iii = 0; iii < leftCurve.size()-1; ++iii)
    {
        indices.push_back(vertices.size());
        indices.push_back(vertices.size()+1);
        indices.push_back(vertices.size()+2);
        indices.push_back(vertices.size());
        indices.push_back(vertices.size()+2);
        indices.push_back(vertices.size()+3);

        glm::vec3 normal(0.0f, 1.0f, 0.0f);
        glm::vec3 color(1.0f, 1.0f, 1.0f);
        color.x = 0.3f;
        color.y = 0.3f;
        color.z = 0.3f;

        glm::vec3 position(0.0f, 0.0f, 0.0f);
        glm::vec2 texPosition(0.0f, 0.0f);

        position=leftCurve[iii];
        texPosition.x = 0.0f;
        texPosition.y = 0.0f;
        vertices.push_back({position, normal, color, texPosition});

        position=rightCurve[iii];
        texPosition.x = 1.0f;
        vertices.push_back({position, normal, color, texPosition});

        position=rightCurve[iii+1];
        texPosition.y = 1.0f;
        vertices.push_back({position, normal, color, texPosition});

        position=leftCurve[iii+1];
        texPosition.x = 0.0f;
        vertices.push_back({position, normal, color, texPosition});
    }

    std::string texturePath="../GeometryMorphing/resources/textures/white.png";

    Texture textures[]
    {
        Texture(texturePath.c_str(), "diffuse", 0, GL_RGBA, GL_UNSIGNED_BYTE)
    };
    std::vector<Texture> tex(textures, textures + sizeof(textures) / sizeof(Texture));

    std::cout<<"Final vertex count: "<<vertices.size()<<std::endl;

    return Mesh(vertices, indices, tex);
}

Mesh MeshFactory::getPartTurnLeft(float width)
{
    glm::vec4 bl(0.0f-width, 0.0f, -1.0f, 1.0f);
    glm::vec4 br(0.0f+width, 0.0f, -1.0f, 1.0f);

    glm::vec4 el(-1.0f, 0.0f, 0.0f-width, 1.0f);
    glm::vec4 er(-1.0f, 0.0f, 0.0f+width, 1.0f);

    glm::vec4 wl(bl.x, 0.0f, el.z, 1.0f);
    glm::vec4 wr(br.x, 0.0f, er.z, 1.0f);

    std::vector<glm::vec4> leftWaypoints;
    leftWaypoints.push_back(bl);
    leftWaypoints.push_back(wl);
    leftWaypoints.push_back(el);

    std::vector<glm::vec4> rightWaypoints;
    rightWaypoints.push_back(br);
    rightWaypoints.push_back(wr);
    rightWaypoints.push_back(er);

    std::vector<glm::vec4> leftCurve = getBezierCurve(leftWaypoints, partResolution);
    std::vector<glm::vec4> rightCurve = getBezierCurve(rightWaypoints, partResolution);

    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;

    for (GLuint iii = 0; iii < leftCurve.size()-1; ++iii)
    {
        indices.push_back(vertices.size());
        indices.push_back(vertices.size()+1);
        indices.push_back(vertices.size()+2);
        indices.push_back(vertices.size());
        indices.push_back(vertices.size()+2);
        indices.push_back(vertices.size()+3);

        glm::vec3 normal(0.0f, 1.0f, 0.0f);
        glm::vec3 color(1.0f, 1.0f, 1.0f);
        color.x = 0.3f;
        color.y = 0.3f;
        color.z = 0.3f;

        glm::vec3 position(0.0f, 0.0f, 0.0f);
        glm::vec2 texPosition(0.0f, 0.0f);

        position=leftCurve[iii];
        texPosition.x = 0.0f;
        texPosition.y = 0.0f;
        vertices.push_back({position, normal, color, texPosition});

        position=rightCurve[iii];
        texPosition.x = 1.0f;
        vertices.push_back({position, normal, color, texPosition});

        position=rightCurve[iii+1];
        texPosition.y = 1.0f;
        vertices.push_back({position, normal, color, texPosition});

        position=leftCurve[iii+1];
        texPosition.x = 0.0f;
        vertices.push_back({position, normal, color, texPosition});
    }

    std::string texturePath="../GeometryMorphing/resources/textures/white.png";

    Texture textures[]
    {
        Texture(texturePath.c_str(), "diffuse", 0, GL_RGBA, GL_UNSIGNED_BYTE)
    };
    std::vector<Texture> tex(textures, textures + sizeof(textures) / sizeof(Texture));

    std::cout<<"Final vertex count: "<<vertices.size()<<std::endl;

    return Mesh(vertices, indices, tex);
}

std::vector<glm::vec4> MeshFactory::getTestLeft(float width)
{
    std::vector<glm::vec4> waypoints;
    waypoints.push_back({0.0f, 0.0f, -10.0f, 1.0f});
    waypoints.push_back({0.0f, 0.0f,   0.0f, 1.0f});
    waypoints.push_back({0.0f, 0.0f,  10.0f, 1.0f});

    //Create two curves, one for each side of the roadpiece
    glm::vec3 translationLeft({-width, 0.0f, 0.0f});
    glm::mat4 translateLeft = glm::translate(glm::mat4(1.0f), translationLeft);

    std::vector<glm::vec4> leftWaypoints;
    leftWaypoints.push_back(translateLeft*waypoints[0]);
    leftWaypoints.push_back(translateLeft*waypoints[1]);
    leftWaypoints.push_back(translateLeft*waypoints[2]);

    std::vector<glm::vec4> leftCurve = getBezierCurve(leftWaypoints, partResolution);

    return leftCurve;
}

std::vector<glm::vec4> MeshFactory::getTestRight(float width)
{
    std::vector<glm::vec4> waypoints;
    waypoints.push_back({0.0f, 0.0f, -10.0f, 1.0f});
    waypoints.push_back({0.0f, 0.0f,   0.0f, 1.0f});
    waypoints.push_back({0.0f, 0.0f,  10.0f, 1.0f});

    glm::vec3 translationRight({width, 0.0f, 0.0f});
    glm::mat4 translateRight = glm::translate(glm::mat4(1.0f), translationRight);

    std::vector<glm::vec4> rightWaypoints;
    rightWaypoints.push_back(translateRight*waypoints[0]);
    rightWaypoints.push_back(translateRight*waypoints[1]);
    rightWaypoints.push_back(translateRight*waypoints[2]);

    std::vector<glm::vec4> rightCurve = getBezierCurve(rightWaypoints, partResolution);

    return rightCurve;
}









































































