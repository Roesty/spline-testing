#ifndef CIRCLE_H
#define CIRCLE_H

#include "mesh.h"

class Circle
{
public:
    Circle();

    Mesh m_mesh;

    glm::vec3 m_position;
    glm::vec3 m_color;

    GLfloat m_radius;

    GLuint m_corners;

    void buildMesh();
};

#endif // CIRCLE_H
