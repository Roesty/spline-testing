#ifndef MESHFACTORY_H
#define MESHFACTORY_H

#include "mesh.h"

#include "glm/gtx/perpendicular.hpp"

class MeshFactory
{
public:
    enum eMesh {
        MESH_GROUNDFLOOR,
        MESH_SPHERE,
        MESH_PART_STRAIGHT,
        MESH_PART_TURN_RIGHT,
        MESH_PART_TURN_LEFT,

        MAX_MESH
    };

    unsigned int partResolution;

    MeshFactory();

    Mesh getRoad(std::vector<glm::vec4> waypoints, GLuint resolution, GLfloat width);

    glm::vec4 getSidePointLeft(glm::vec4 point, glm::vec4 tangent, GLfloat n=1.0f);
    glm::vec4 getSidePointRight(glm::vec4 point, glm::vec4 tangent, GLfloat n=1.0f);

    glm::vec4 centripetalSplineTangent(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2, glm::vec4 p3, GLfloat t, GLfloat a);
    glm::vec4 splineTangent(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2, glm::vec4 p3, GLfloat t);

    glm::vec4 centripetalSpline(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2, glm::vec4 p3, GLfloat t, GLfloat a);
    glm::vec4 spline(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2, glm::vec4 p3, GLfloat t);

    glm::vec4 lerpV4(glm::vec4 p0, glm::vec4 p1, float t);
    GLfloat lerpf(GLfloat x, GLfloat y, GLfloat t);

    glm::vec4 getBezier3(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2, GLfloat t);
    glm::vec4 getBezier4(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2, glm::vec4 p3, GLfloat t);
    glm::vec4 getBezierN(std::vector<glm::vec4> points, GLfloat t);
    glm::vec4 getBezierV2(std::vector<glm::vec4> points, float t);

    glm::vec4 getBezier(std::vector<glm::vec4> points, float t);
    glm::vec4 getBezier(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, float t);

    std::vector<glm::vec4> getBezierCurve(std::vector<glm::vec4> points, GLuint steps);

    glm::vec4 quadBezier(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, GLfloat t);
    glm::vec4 quadBezier(std::vector<glm::vec4> points, GLfloat t);

    glm::vec4 quadBezierDer(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, GLfloat t);
    glm::vec4 quadBezierDer(std::vector<glm::vec4> points, GLfloat t);

    glm::vec4 quadBezierPer(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, GLfloat t, GLfloat n);
    glm::vec4 quadBezierPer(std::vector<glm::vec4> points, GLfloat t, GLfloat n);

    std::vector<glm::vec4> quadBezierCurve(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, GLuint steps);
    std::vector<glm::vec4> quadBezierCurve(std::vector<glm::vec4> points, GLuint steps);

    glm::vec4 cubeBez(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, GLfloat t);
    glm::vec4 cubeBezDer(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, GLfloat t);
    glm::vec4 cubeBezPer(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, GLfloat t, GLfloat o);

    Mesh getGroundFloor(int length, int width, float size,
                        glm::vec3 c1={0.1, 0.1, 0.1},
                        glm::vec3 c2={0.05, 0.05, 0.05});

    Mesh getSphere(float radius=1.0f, glm::vec3 c={1, 1, 1});

    Mesh getPartStraight(float width=0.5f);
    Mesh getPartTurnRight(float width=0.5f);
    Mesh getPartTurnLeft(float width=0.5f);

    std::vector<glm::vec4> getTestLeft(float width=0.9f);
    std::vector<glm::vec4> getTestRight(float width=0.9f);

};

#endif // MESHFACTORY_H
